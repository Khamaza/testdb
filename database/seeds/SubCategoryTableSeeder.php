<?php

use Illuminate\Database\Seeder;

class SubCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(static::$clothingTypes as $type)
        {
            factory(\App\Subcategory::class, 1)->create([
                'name' => $type
            ]);
        }
    }

    protected static $clothingTypes = [
        'pants', 'skirt', 'trousers', 't-shirt', 'socks', 'sweat shirt', 'jacket', 'polo', 'shorts',
        'sweatpants', 'dress', 'costume', 'apron', 'suit', 'bathing trousers', 'bikini',
        'blouse', 'body stocking', 'bodysuit', 'coat', 'dressing gown', 'gilet', 'gloves', 'stockings',
        'jacket', 'jumper', 'jump suit', 'kimono', 'leotard', 'cloak', 'mantle', 'nightdress', 'night gown',
        'overcoat', 'overskirt', 'peignoir', 'pullover', 'pyjamas', 'sarong', 'shirt', 'trunks',
    ];
    
}
