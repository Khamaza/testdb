<?php

use Illuminate\Database\Seeder;

class SeasonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        foreach(static::$seasons as $season)
        {
            factory(\App\Season::class, 1)->create([
                'season' => $season
            ]);
        }
    }

    protected static $seasons = [
        'winter',
        'autumn',
        'summer',
        'spring'
    ];
}