<?php

use Illuminate\Database\Seeder;

class CartTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i <=2000; $i++)
        {
            factory(\App\Cart::class, 1)->create([
                'product_id' => $this->getRandomProductId()
            ]);
        }
    }

    private function getRandomProductId() {
        $product = \App\Product::inRandomOrder()->first();
        return $product->id;
    }
}
