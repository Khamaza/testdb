<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        for($i = 0; $i <=2000; $i++)
        {
            factory(\App\Product::class, 1)->create([
                'category_id' => $this->getRandomCategoryId(),
                'size_id' => $this->getRandomSizeId(),
                'season_id' => $this->getRandomSeasonId(),
                'subcategory_id' => $this->getRandomSubcategoryId(),
                'manufactur_id' => $this->getRandomManufacturId()
            ]);
        }
    }

    private function getRandomCategoryId() {
        $category = \App\Category::inRandomOrder()->first();
        return $category->id;
    }

    private function getRandomSizeId() {
        $size = \App\Size::inRandomOrder()->first();
        return $size->id;
    }

    private function getRandomSeasonId() {
        $season = \App\Season::inRandomOrder()->first();
        return $season->id;
    }

    private function getRandomSubcategoryId() {
        $subcategory = \App\Subcategory::inRandomOrder()->first();
        return $subcategory->id;
    }

    private function getRandomManufacturId() {
        $manufactur = \App\Manufactur::inRandomOrder()->first();
        return $manufactur->id;
    }
}
