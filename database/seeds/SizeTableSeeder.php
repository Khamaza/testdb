<?php

use Illuminate\Database\Seeder;

class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(static::$sizes as $size)
        {
            factory(\App\Size::class, 1)->create([
                'size' => $size
            ]);
        }
    }

    protected static $sizes =  [
        'XS',
        'S',
        'M',
        'L',
        'XL',
        'XXL'
    ];
}