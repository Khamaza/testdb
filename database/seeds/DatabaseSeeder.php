<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {   
        $this->call(UserTableSeeder::class);
        $this->call(BlogTableSeeder::class);
        $this->call(ManufacturesTableSeeder::class);
        $this->call(SubCategoryTableSeeder::class);
        $this->call(SeasonTableSeeder::class);
        $this->call(SizeTableSeeder::class);
        $this->call(CategoryTableSeeder::class);

        $this->call(ProductTableSeeder::class);
        $this->call(CartTableSeeder::class);
    }
}
