<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        foreach(static::$categories as $category)
        {
            factory(\App\Category::class, 1)->create([
                'name' => $category
            ]);
        }
    }

    protected static $categories = [
        'Men',
        'Women',
        'Kid'
    ];
    
}
