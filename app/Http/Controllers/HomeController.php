<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Faker\Generator as Faker;

class HomeController extends Controller
{
    protected static $clothingTypes = [
        'pants', 'skirt', 'trousers', 't-shirt', 'socks', 'sweat shirt', 'jacket', 'polo', 'shorts',
        'sweatpants', 'dress', 'costume', 'apron', 'suit', 'bathing trousers', 'bikini',
        'blouse', 'body stocking', 'bodysuit', 'coat', 'dressing gown', 'gilet', 'gloves', 'stockings',
        'jacket', 'jumper', 'jump suit', 'kimono', 'leotard', 'cloak', 'mantle', 'nightdress', 'night gown',
        'overcoat', 'overskirt', 'peignoir', 'pullover', 'pyjamas', 'sarong', 'shirt', 'trunks',
    ];

    public function index(){
        // $faker = \Faker\Factory::create();
        // $faker->addProvider(new Clothing($faker));
        
        // foreach (range(1,10) as $i) {
        //     dd();
        // }
    
    }
}
